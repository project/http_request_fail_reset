Http Request Fail Reset
http://drupal.org/project/http_request_fail_reset
=================================================


DESCRIPTION
------------
At Tribute Media we have many sites in various stages of development. Sometimes upon returning to them I have noticed that Update Status has not checked for updates for up to 4 weeks. I then notice that the "drupal_http_request_fails" flag is set.

It is my understanding that when the variable "drupal_http_request_fails" is set the only way to get Drupal to re-attempt to make a request if to manually reset the variable. Drupal doesn't even check anymore until I reset the flag.

This module allows the user to configure when to check if the flag is set, and reset the flag. The interval is configurable from 1 minute to days or weeks.

When the interval times out the module checks "drupal_http_request_fails" to see if it is set to TRUE - meaning that no HTTP requests are being attempted - this module will reset the flag and log to the watchdog table. If the variable is set to FALSE - meaning that there is no problems making HTTP requests - it logs this to the watchdog table as well.

There are many things that can cause the "drupal_http_request_fails" flag to be set to TRUE, one of these is having the site offline. Drupal tries to make a connection to itself to make sure it can make http requests, if the site is offline it gets the Maintenance page instead of the page it is expecting, so it thinks it cannot make http requests.


REQUIREMENTS
------------
Drupal 6.x


INSTALLING
------------
Copy the 'http_request_fail_reset' folder to your sites/all/modules directory.
Go to Administer > Site building > Modules. Enable the module.
Read more about installing modules at http://drupal.org/node/70151


CONFIGURING AND USING
---------------------
Go to http://yourdomainnamehere.com/cron.php
This will start the automated CRON process.

If successful Drupal will return a blank page. With the message '$fails = TRUE'.

Go to http://yourdomainnamehere.com/admin
Navigate to ADMINISTER > REPORTS > STATUS REPORT. Error message should be gone.


REPORTING ISSUE. REQUESTING SUPPORT. REQUESTING NEW FEATURE
-----------------------------------------------------------
1. Go to the module issue queue at http://drupal.org/project/issues/http_request_fail_reset?status=All&categories=All
2. Click on CREATE A NEW ISSUE link.
3. Fill the form.
4. To get a status report on your request go to http://drupal.org/project/issues/user


UPGRADING
---------
1. One of the most IMPORTANT things to do BEFORE you upgrade, is to backup your site's files and database. More info: http://drupal.org/node/22281
2. Disable actual module. To do so go to Administer > Site building > Modules. Disable the module.
3. Just overwrite (or replace) the older module folder with the newer version.
4. Enable the new module. To do so go to Administer > Site building > Modules. Enable the module.
5. Run the update script. To do so go to the following address: www.yourwebsite.com/update.php
Follow instructions on screen. You must be log in as an administrator (user #1) to do this step.

Read more about upgrading modules: http://drupal.org/node/250790
